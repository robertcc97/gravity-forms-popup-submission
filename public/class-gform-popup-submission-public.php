<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.onyxdigitaldesign.com
 * @since      1.0.0
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/public
 * @author     Robert Crawford <info@onyxdigitaldesign.com>
 */
class Gform_Popup_Submission_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gform_Popup_Submission_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gform_Popup_Submission_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gform-popup-submission-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gform_Popup_Submission_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gform_Popup_Submission_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gform-popup-submission-public.js', array( 'jquery' ), $this->version, false );

	}



public function ag_custom_confirmation( $confirmation, $form, $entry, $ajax ) {
	$form_ids = get_option('_gfp_forms');
	//if (in_array($form['id'], $form_ids)) {
		add_action( 'wp_footer',array($this, 'ag_overlay'));
	return '<div id="gform-notification">' . $confirmation . '<a class="button" href="#">OK</a></div>';
//	}	
}

public function ag_overlay() {
	echo '<div id="overlay"></div>';
	echo '
		<script type="text/javascript">
			jQuery("body").addClass("message-sent");
			jQuery("#gform-notification a").click(function() {
				jQuery("#overlay,#gform-notification").fadeOut("normal", function() {
					$(this).remove();
				});
			});
		</script>
	';
}

}
add_filter( 'gform_confirmation', 'ag_custom_confirmation', 10, 4 );
function ag_custom_confirmation( $confirmation, $form, $entry, $ajax ) {
	$form_ids = get_option('_gfp_forms');
	if (in_array($form['id'], $form_ids)) {
	add_action( 'wp_footer', 'ag_overlay');
	$thisform = $form['id'];
	return '[gravityform id=' . $thisform . ' title=false description=false]<div id="gform-notification">' . $confirmation . '<a class="button" href="#">OK</a></div>';
	}
	else {
		return $confirmation . '<a class="button" href="#">OK</a></div>';
	}
}

/* Add script to remove the overlay and confirmation message once the button in the popup is clicked.
----------------------------------------------------------------------------------------*/
function ag_overlay() {
	echo '<div id="overlay"></div>';
	echo '
		<script type="text/javascript">
			jQuery("body").addClass("message-sent");
			jQuery("#gform-notification a").click(function() {
				jQuery("#overlay,#gform-notification").fadeOut("normal", function() {
					$(this).remove();
				});
			});
		</script>
	';
	?>
	<style>
 /*#overlay {
	background: #000;
	background: rgba(0, 0, 0, 0.3);
	display: block;
	float: left;
	height: 100%;
	position: fixed;
	top: 0; left: 0;
	width: 100%;
	z-index: 99;
}*/

#gform-notification {
	background: #fff;
	border-radius: 10px;
	display: block;
	margin: auto;
	max-height: 237px;
	max-width: 520px;
	padding: 61px;
	position: fixed;
	top: 0; left: 0; right: 0; bottom: 10vh;
	text-align: center;
	width: 100%;
	z-index: 10000 !important;
	-webkit-box-shadow: 3px 2px 16px 0px rgba(120,114,120,1);
-moz-box-shadow: 3px 2px 16px 0px rgba(120,114,120,1);
box-shadow: 3px 2px 16px 0px rgba(120,114,120,1);
}

#gform-notification .button {
	margin: 20px 0 0;
	padding: 12px 24px;
}
</style>
	<?php
}


    
add_action( 'admin_menu', 'twill_product_slideout_menu' );

function twill_product_slideout_menu(){

  $page_title = 'Gravity Form Popup';
  $menu_title = 'GF Popup';
  $capability = 'manage_options';
  $menu_slug  = 'gravity-form-popup';
  $function   = 'gravity_form_popup';
  $icon_url   = 'dashicons-media-code';
  $position   = 16;

  add_menu_page( $page_title,
                 $menu_title, 
                 $capability, 
                 $menu_slug, 
                 $function, 
                 $icon_url, 
                 $position );
}

function gravity_form_popup() {
  $user = wp_get_current_user();
  $forms = get_option( '_gfp_forms');
    ?>
	<style>
	.btn {
	background-color:#7892c2;
	-moz-border-radius:28px;
	-webkit-border-radius:28px;
	border-radius:28px;
	border:1px solid #4e6096;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:17px;
	padding:8px 35px;
	text-decoration:none;
	text-shadow:0px 1px 0px #283966;
}
.btn:hover {
	background-color:#476e9e;
}
.btn:active {
	position:relative;
	top:1px;
}

	</style>
    <div class="container" style="margin:15px; padding: 15px;">
    <div class="title">
    <h3> 
    Gravity Forms Options Menu
    </h3>
    </div>
    <div class="forms-input">
<h3 class='g-title'> Please add the id's of the form that you wish to display the popup on </h3>
  <div class="input">
    <h5>
    Please Add Comma Seperated Gravity Form ID's : <input class="gform-ids" type="text" />
    </h5>
    </div>
  <!--  <div class="button-add-input">
    <button class="c-add"> Add Color </button>
    </div> -->
    </div>
        <button class="btn btn-info save-color"> Save Settings </button>
    </div>


    <script>
    jQuery(document).ready(function($) {
		<?php 
			
				$tags = implode(', ', $forms);
					?>
					$('.gform-ids').val('<?php echo $tags; ?>');
					<?php
		
			?>

        $('.save-color').click(function() {
			var string = $('.gform-ids').val();
          var inputValues = string.split(',');
            console.log(inputValues);
            var gids = JSON.stringify(inputValues);
            console.log(gids);
            setTimeout(function(){
           $.ajax({
                url: ajaxurl,
                type: "POST",
                data: {action : 'gforms_popup_options_update', gforms_id: gids},
                dataType: "json",
                success: function(data) {
                    alert(data);
                }
              }); 
            }, 2000);
        });
    });
    </script>
    <?php
}

add_action( 'wp_ajax_gforms_popup_options_update', 'gforms_popup_options_update' );
function gforms_popup_options_update() {
		 $forms = json_decode( stripslashes($_POST['gforms_id']));
		 echo $forms;
       echo update_option( '_gfp_forms', $forms );

exit();
}
