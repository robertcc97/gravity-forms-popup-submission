<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.onyxdigitaldesign.com
 * @since      1.0.0
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 * @author     Robert Crawford <info@onyxdigitaldesign.com>
 */
class Gform_Popup_Submission_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
