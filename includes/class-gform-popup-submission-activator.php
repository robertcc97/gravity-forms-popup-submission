<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.onyxdigitaldesign.com
 * @since      1.0.0
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 * @author     Robert Crawford <info@onyxdigitaldesign.com>
 */
class Gform_Popup_Submission_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
