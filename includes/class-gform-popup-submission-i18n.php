<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.onyxdigitaldesign.com
 * @since      1.0.0
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/includes
 * @author     Robert Crawford <info@onyxdigitaldesign.com>
 */
class Gform_Popup_Submission_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gform-popup-submission',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
