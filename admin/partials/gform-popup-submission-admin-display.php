<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.onyxdigitaldesign.com
 * @since      1.0.0
 *
 * @package    Gform_Popup_Submission
 * @subpackage Gform_Popup_Submission/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
